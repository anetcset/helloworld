﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HelloWorld3;

namespace HelloWorldTest3
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual("Hello World3", Program.getMessage());
        }
    }
}
